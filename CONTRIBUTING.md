# Contributing guide

# Setup poetry

- Follow instructions for "Initializing pre-existing project" by official poetry [documentation](https://python-poetry.org/docs/basic-usage/#initialising-a-pre-existing-project)
- Create and activate [virtual environment](https://python-poetry.org/docs/basic-usage/#activating-the-virtual-environment)
```bash
poetry shell
```
- [Install dependencies](https://python-poetry.org/docs/basic-usage/#installing-dependencies)
```bash
poetry install
```

# Setup pre-commit

- run installation command
```bash
pre-commit install
```
- check if pre-commit is working fine
```bash
pre-commit run --all-files
```

Due to pre-commit configuration from **pre-commit-config.yaml** now you have as dev tools:
- black
- isort
- flake8
- mypy
