FROM python:3.11-slim

#setup workdir
WORKDIR /ml-app

# setup poetry
RUN pip install poetry==1.7.1

# install dependencies
COPY pyproject.toml poetry.lock /ml-app/
RUN --mount=type=cache,target=/root/.cache \
    poetry install --no-root

# copy other project files
COPY src /ml-app/src

# copy tests
# TODO: add (probably) functional tests

RUN poetry install

# Setup entrypoint
# TODO: change "env info" to "run" when repo get a code to run
ENTRYPOINT ["poetry", "env info"]
