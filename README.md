# MLOps Course Repo

# Methodology Chosen
**Github Workflow**

# Docker instructions
 - Install Docker with [instructions](https://docs.docker.com)
 - [Start](https://docs.docker.com/config/daemon/start/) docker daemon
 - Make sure that you have activated your poetry environment
```bash
poetry shell
```
 - Then build an app image. You can skip this part if you already have an image build with actual state of dependencies.
```bash
docker build . -t ml-app
```
- Now you can test it with
```bash
docker run -it ml-app
```
